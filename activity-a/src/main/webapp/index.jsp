<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.text.SimpleDateFormat"
    import="java.util.Date"
    import="java.util.Locale"
	import="java.util.TimeZone"%>
<!-- Import the "java.time.*" and "java.time.format.DateTimeFormatter" -->
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>JSP Activity</title>
</head>
<body>

	<!-- Using the scriptlet tag, create a variable dateTime-->
	<!-- Use the LocalDateTime and DateTimeFormatter classes -->
	<!-- Change the pattern to "yyyy-MM-dd HH:mm:ss" -->
  	
  	<%
  	SimpleDateFormat dateTime = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
	TimeZone timeZone1 = TimeZone.getTimeZone("Asia/Manila");
	dateTime.setTimeZone(timeZone1);
	Date manila = new Date();
	String currentManilaDateTime = dateTime.format(manila);
	TimeZone timeZone2 = TimeZone.getTimeZone("Japan");
	dateTime.setTimeZone(timeZone2);
	Date japan = new Date();
	String currentJapanDateTime = dateTime.format(japan);
	TimeZone timeZone3 = TimeZone.getTimeZone("Europe/Berlin");
	dateTime.setTimeZone(timeZone3);
	Date germany = new Date();
	String currentGermanyDateTime = dateTime.format(germany);
	%>
  	<!-- Using the date time variable declared above, print out the time values -->
  	<!-- Manila = currentDateTime -->
  	<!-- Japan = +1 -->
  	<!-- Germany = -7 -->
  	<!-- You can use the "plusHours" and "minusHours" method from the LocalDateTime class -->
  	<h1>Our Date and Time now is...</h1>
	<ul>
		<li> Manila: <%= currentManilaDateTime %></li>
		<li> Japan: <%= currentJapanDateTime %></li>
		<li> Germany: <%= currentGermanyDateTime %></li>
	</ul>
	
	<!-- Given the following Java Syntax below, apply the correct JSP syntax -->
	<%!
	private int initVar=3;
	private int serviceVar=3;
	private int destroyVar=3;
	 %>
	<%! 
  		public void jspInit(){
    		initVar--;
    		System.out.println("jspInit(): init"+initVar);
  		}
  		public void jspDestroy(){
    		destroyVar--;
    		destroyVar = destroyVar + initVar;
    		System.out.println("jspDestroy(): destroy"+destroyVar);
  		}
  	%>
  	<%
  		serviceVar--;
  		System.out.println("_jspService(): service"+serviceVar);
  		String content1="content1 : "+initVar;
  		String content2="content2 : "+serviceVar;
  		String content3="content3 : "+destroyVar;
  	 %>
	<h1>JSP</h1>
	<p><%= content1 %></p>
	<p><%= content2 %></p>
	<p><%= content3 %></p>

</body>
</html>